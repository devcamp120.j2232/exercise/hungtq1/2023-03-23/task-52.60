package Package_User;

public class User_detail {
    
    String password;
    String userName;
    boolean enabled;
    
    public String toString(){
        return "User_Details: " + password + " " + userName + " " + enabled;
    }

    public User_detail() {
        
    }

    public User_detail(String password, String userName, boolean enabled) {
        this.password = password;
        this.userName = userName;
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
